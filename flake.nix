{
  description = "buildRustPackage - but based on `import-cargo'";

  inputs.import-cargo.url = github:edolstra/import-cargo;

  outputs = { self, import-cargo }: {
    builders.buildRustPackage =
      { pkgs }:
      { pname, version, src
      , type ? "release"
      , lockFile ? (src + "/Cargo.lock")
      , nativeBuildInputs ? []
      , meta ? {}
      , ...
      }@args:
        let
          target = pkgs.rust.toRustTarget pkgs.stdenv.hostPlatform;
          flags = "--release --offline --target ${target}";
          inherit (import-cargo.builders) importCargo;
        in pkgs.stdenv.mkDerivation ((builtins.removeAttrs args [ "type" "lockFile" ]) // {
          nativeBuildInputs = nativeBuildInputs ++ (with pkgs; [
            rustc cargo
            (importCargo { inherit lockFile pkgs; }).cargoHome
            pkg-config
          ]);

          outputs = [ "out" "doc" ];
          separateDebugInfo = true;
          doCheck = true;

          buildPhase = ''
            runHook preBuild

            export RUSTFLAGS='-g'
            cargo build ${flags} -j $NIX_BUILD_CORES
            cargo doc ${flags}

            runHook postBuild
          '';

          checkPhase = ''
            runHook preCheck

            cargo test ${flags} -j $NIX_BUILD_CORES

            runHook postCheck
          '';

          installPhase = ''
            runHook preInstall

            mkdir -p $out/bin
            mkdir -p $doc/share/doc/${pname}

            cp ./target/${target}/release/${pname} $out/bin/
            cp -r ./target/${target}/doc $doc/share/doc/${pname}

            runHook postInstall
          '';

          meta = { mainProgram = pname; } // meta;
        });
  };
}
