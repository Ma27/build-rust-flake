# buildRustPackage - but based on `import-cargo'

Opinionated [flake](https://nixos.wiki/wiki/Flakes) which builds Rust code based on
[`edolstra/import-cargo`](https://github.com/edolstra/import-cargo).

## Usage

```nix
{
  inputs = {
    nixpkgs.url = github:NixOS/nixpkgs/release-21.05;
    build-rust-flake.url = path:/home/ma27/Projects/build-rust-flake;
  };
  outputs = { self, nixpkgs, build-rust-flake }: {
    overlay = final: prev: let
      buildRustCode = build-rust-flake.builders.buildRustPackage { pkgs = final; };
    in {
      my-rust-project = buildRustCode {
        pname = "my-rust-project";
        version = "0.1";

        # Assumes that a `Cargo.lock` exists
        src = final.lib.cleanSource ./.;

        # E.g. for `openssl-sys`.
        buildInputs = [ final.openssl ];
      };
    };
  };
}
```

* Assumes a binary called `pname` (i.e. `my-rust-project` in this case) that will be installed
  into `$out/bin`.
* Assumes documentation from `pname` that will be installed into `$doc/share/doc/${pname}`.
* Debug symbols are included by default, but stripped and copied into `$debug`.
